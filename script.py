# This script implements the creation and management of a SQL
# database using Python 3.
# This program is for observing the crow on our terrace:
# - is it actually the crow, or maybe some other bird?
# - how many times a day the crow visited to us
# - the crow arrival and departure times
# - where the crow left her turd
#
# Using the command line, it will be possible to create a
# database, make records, (delete erroneous records??? Make edits
# in existing ones???)


import argparse
import sqlite3
from sqlite3 import Error
from datetime import date
from datetime import datetime
import re


def create_connection(path):
    """ create a database connection to the SQLite database specified by path,
    or create new database file, if such does not exist.
    :param path: database file
    :return: Connection object or None """
    connection = None
    try:
        connection = sqlite3.connect(path)
        print("Connection to SQLite DB successful")
    except Error as e:
        print(f"The error occurred: '{e}'")

    return connection


def execute_query(connection, query):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        print("Query executed successfully")
    except Error as e:
        print(f"The error occurred: '{e}'")


# queries
create_table = '''CREATE TABLE IF NOT EXISTS birds_2
               (
               bird_name text,
               time_start text UNIQUE,
               time_end text UNIQUE,
               where_turd text
               );'''

# date
tod = date.today()
today = tod.strftime("%Y-%m-%d")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Program to count visits of the crow')
    parser.add_argument('-v', '--verbose',
                        action="store_true",
                        help="more verbosity output")
    parser.add_argument('-d', '--date',
                        help="Format: YYYY-MM-DD. Specifies date of the record. Default is today.",
                        default=today)
    parser.add_argument('bird_name')
    parser.add_argument('time_start', help='When the bird arrived. Format: HH:MM')
    parser.add_argument('time_end', help='When the bird flew away. Format: HH:MM')
    parser.add_argument('where_turd', help="Where the bird make a poo. Available options: floor, chairs, handrail, table, no_turd")
    args = parser.parse_args()

    def debug():
        print(f"""
              --date: {args.date}
              bird_name: {args.bird_name}
              time_start: {args.time_start}
              time_end: {args.time_end}
              where_turd: {args.where_turd}
              --verbose: {args.verbose}
              """)
    #debug()

    ## date bird_name time_start time_end where_turd
    ## сегодня ворона 16:00 16:01 поручень

    # conditions
    turd_list = ['floor', 'chairs', 'handrail', 'table', 'no_turd']

    if args.where_turd not in turd_list:
        print("where_turd argument mist be one of the following words: floor, chairs, handrail, table, no_turd - if the bird didn’t poop")
        exit()

    # ToDo: links to the table
    # fix time limits

    format_string = '%H:%M'
    try:
        x = datetime.strptime(date_string, format_string)
        print(x)
    except ValueError as e:
        print(f'Dude, you got an error: \n{e}')




    connection = create_connection("birds.db")
    execute_query(connection, create_table)

    query_add_record = f"""
    INSERT OR IGNORE INTO birds_2 VALUES (
        '{args.bird_name}',
        '{today+' '+args.time_start}',
        '{today+' '+args.time_end}',
        '{args.where_turd}')
    """
    execute_query(connection, query_add_record)

    #print(today)

